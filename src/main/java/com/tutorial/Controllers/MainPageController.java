/**
 * Created by zeruch on 12/21/16.
 */
package com.tutorial.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainPageController {

    @RequestMapping("/")
    String index() {
        System.err.println("hi");
        return "index";
    }
}
